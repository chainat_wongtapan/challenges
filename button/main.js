function getRowAndParent(button) {
  return {row: button.parentNode, wrapper: button.parentNode.parentNode};
}

function addRow(button) {
  const {row, wrapper} = getRowAndParent(button);
  const newRow = row.cloneNode(true);
  wrapper.insertBefore(newRow, row.nextSibling);
}

function removeRow(button) {
  const {row, wrapper} = getRowAndParent(button);
  wrapper.removeChild(row);
}

function init() {
  document.getElementById('item-holder').addEventListener('click', function (e) {
      e.preventDefault();
      const target = e.target;
      if (target.nodeName === 'BUTTON') {
        (target.innerText === 'Add') ? addRow(target) : removeRow(target);
      }
      return false;
  });
}

window.onload = init;
