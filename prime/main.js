const MAX = 1000;

function isPrime(number) {
  // If not 2, and even number
  const two = (number === 2);
  const even = (number % 2 === 0);
  let devidable = false;
  if (!even) {
    const half = Math.floor(number/2);
    let i = 2;

    // Validate the first half
    while(!devidable && i <= half) {
      other = (number % i) === 0;
      i+=1;
    }
  }
  return (number > 1) && ( two || (!even && !devidable) );
}


function isFibonacci(number) {
  let prevFib = 1, nextFib = 2;
  let isFib = false;
  while (nextFib < number && !isFib) {
    const tmp = prevFib;
    prevFib = nextFib;
    nextFib = tmp + nextFib;
    isFib = number == nextFib;
  }
  return isFib;
}

function getPairOfPrimes(number) {
  let hasPair = false;
  const pairOfPrimes = [];
  const possibleNumbers = new Array(number).fill().map((e,i) => i + 1);
  const primeNumbers = possibleNumbers.filter((n) => isPrime(n));
  const length = primeNumbers.length;

  for(let i = 0; i < length; i+=1) {
    for(let j = i + 1; j < length; j+=1) {
      if (primeNumbers[i] + primeNumbers[j] === number) {
        hasPair = true;
        pairOfPrimes.push([primeNumbers[i], primeNumbers[j]]);
      }
    }
  }
  return {hasPair, pairOfPrimes};
}

// Task 1
console.log('TASK 1\n--------------');
for(let i=1; i<MAX; i+=1) {
  const isFib = isFibonacci(i);
  isPrime(i) && isFib ? console.log(i) : false;
}

// Task 2
console.log('\nTASK 1 (modified)\n--------------');
console.log('Fib\tPrim1\tPrim2\n------- ------- -------');
for(let i=1; i<MAX; i+=1) {
  const isFib = isFibonacci(i);
  if (isFib) {
    const { hasPair, pairOfPrimes } = getPairOfPrimes(i);
    if (hasPair) {
      const out = pairOfPrimes.map((value) => `${i}\t${value[0]}\t${value[1]}`);
      console.log(out.join('\n'));
    }
  }
}
